#!/usr/bin/make -f
#
# (C) 1999-2020 Roland Rosenfeld <roland@debian.org>, based on
# Sample debian/rules that uses debhelper.
# This file is public domain software, originally written by Joey Hess.
#
# This version is for packages that are architecture independent.

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

DINGDIR=`pwd`/debian/ding
TRANSDIR=`pwd`/debian/trans-de-en
DICTDIR=`pwd`/debian/dict-de-en
DICTVERSION:=$(shell sed 's/^.*:: //;1q' de-en.txt)

include /usr/share/dpkg/pkg-info.mk

%:
	dh $@

override_dh_auto_build:
#       Build dictionaries for dictd daemon
	chmod 755 debian/conv.pl
	./debian/conv.pl < de-en.txt \
	    | dictfmt -j --utf8 --without-time \
		-u 'https://dict.tu-chemnitz.de/' \
		-s "German - English Dictionary $(DICTVERSION)" \
		german-english
	dictzip german-english.dict
	./debian/conv.pl -r < de-en.txt \
	    | dictfmt -j --utf8 --without-time \
		-u 'https://dict.tu-chemnitz.de/' \
		-s "English - German Dictionary $(DICTVERSION)" \
		english-german
	dictzip english-german.dict

override_dh_auto_install:
	sed -e 's#/usr/share/dict/de-en.txt#/usr/share/trans/de-en#' ding \
		> $(DINGDIR)/usr/bin/ding
	chmod 755 $(DINGDIR)/usr/bin/ding

	install -m644 ding.1 $(DINGDIR)/usr/share/man/man1/ding.1
	install -m644 ding.desktop \
		$(DINGDIR)/usr/share/applications/ding.desktop

	convert ding.png -geometry 32x32 -colors 6 \
		$(DINGDIR)/usr/share/pixmaps/dbook.xpm
	install -m644 debian/mini-dbook.xpm \
		$(DINGDIR)/usr/share/pixmaps/mini-dbook.xpm
	install -m644 ding.png $(DINGDIR)/usr/share/icons/hicolor/32x32/apps/
	convert ding.png -geometry 64x64 \
		$(DINGDIR)/usr/share/icons/hicolor/64x64/apps/ding.png
	install -m644 debian/html/* $(DINGDIR)/usr/share/doc/ding/html/

	env -u LANG LC_ALL=C MANWIDTH=80 MAN_DISABLE_SECCOMP=1 \
		man -l -7 -P /bin/cat ding.1 | col -b \
		> $(DINGDIR)/usr/share/doc/ding/html/ding.1.txt

	install -m755 -d $(TRANSDIR)/usr/share/trans
	install -m644 de-en.txt $(TRANSDIR)/usr/share/trans/de-en

	install -m 644 debian/english-german-conf.xml \
		$(DICTDIR)/usr/share/opendict/dictionaries/plain/english-german.dict.dz/conf/config.xml
	install -m 644 debian/german-english-conf.xml \
		$(DICTDIR)/usr/share/opendict/dictionaries/plain/german-english.dict.dz/conf/config.xml

override_dh_clean:
	-rm -f *.dz *.index *.dict *.dictfmt
	dh_clean
