Source: ding
Section: text
Priority: optional
Maintainer: Roland Rosenfeld <roland@debian.org>
Standards-Version: 4.6.0
Build-Depends: debhelper-compat (= 13), imagemagick
Build-Depends-Indep: bsdextrautils (>= 2.35.2-3) | bsdmainutils (<< 12.1.1),
                     dictfmt (>= 1.10.1),
                     dictzip,
                     man-db
Homepage: https://www-user.tu-chemnitz.de/~fri/ding/
Vcs-Git: https://salsa.debian.org/debian/ding.git
Vcs-Browser: https://salsa.debian.org/debian/ding
Rules-Requires-Root: no

Package: ding
Architecture: all
Depends: tk, ${misc:Depends}
Recommends: glimpse (>= 4.18.7-6) | tre-agrep,
            trans-de-en (>= 1.4) | translation-dictionary
Breaks: trans-de-en (<< 1.4)
Suggests: dict, hunspell, hunspell-de-de, hunspell-en-us
Multi-Arch: foreign
Description: Graphical dictionary lookup program for Unix (Tk)
 This is "Ding"
  * a dictionary lookup program for Unix,
  * DIctionary Nice Grep,
  * a Tk based Front-End to [ae]grep, (hun|a|i)spell, dict, ...
  * Ding {n} :: thing
 .
 This package needs tre-agrep(1), agrep(1) or egrep(1) as a back end.
 (tre-)agrep is preferable, because it supports fault tolerant
 searching.
 .
 You have to install some translation dictionary word list with a
 word/phrase in two languages in one line with some kind of separator
 between them.  The default configuration of ding uses the
 German-English dictionary which can be found in the trans-de-en
 package, but you can use every other translation word lists with one
 entry per line.

Package: trans-de-en
Architecture: all
Depends: ${misc:Depends}
Provides: translation-dictionary
Suggests: ding
Multi-Arch: foreign
Description: German-English translation dictionary
 A German-English dictionary with ca. 380,000 entries.
 .
 This dictionary was designed for the "ding" dictionary lookup
 program, but may be used by other clients, too.
 .
 The source of the database is available from
 https://dict.tu-chemnitz.de/

Package: dict-de-en
Architecture: all
Depends: ${misc:Depends}
Suggests: dict | dict-client, dictd | dict-server
Breaks: dictd (<< 1.10.1)
Provides: dictd-dictionary
Description: German-English translation dictionary for dictd
 German-English and  English-Deutsch translation dictionary for
 the dictd server.  It contains approximately 380,000 entries.
 .
 The source of the database is available from
 https://dict.tu-chemnitz.de/
